+++
author = "Zuhair Yahya"
title = "Install Telegraf"
date = "2021-02-23"
description = "Cara Install Telegraf"
tags = [
    "Grafana",
    "Monitoring",
    "InfluxDB",
]
categories = [
    "DevOps",
]
+++

Pada artikel kelima ini akan dijelaskan mengenai cara menginstall telegraf

## Install Telegraf

1. Update repo
```shell
yum update
```
2. Install telegraf
```shell
yum install telegraf
```
3. Enable telegraf
```shell
systemctl enable telegraf
```
4. Start telegraf
```shell
systemctl start telegraf
```
5. Edit file config
```shell
vi /etc/telegraf/telegraf.conf
```
6. Cek status telegraf
```shell
systemctl status telegraf
```

Mungkin itu saja yang bisa saya dijelaskan, semoga bermanfaat untuk anda semua