+++
author = "Zuhair Yahya"
title = "Install Minikube"
date = "2021-02-15"
description = "Cara Install Minikube"
tags = [
    "Minikube",
    "Linux",
    "Container",
]
categories = [
    "DevOps",
]
thumbnail= "images/minikube.jpg"
+++

Pada artikel pertama ini, akan dijelaskan mengenai cara menginstall minikube di Linux

## Install Minikube

1. Install minikube menggunakan unduhan langsung
```shell
curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
```
2. Jadikan biner minikube dapat dieksekusi
```shell
chmod +x minikube
```
3. Tambahkan program minikube kedalam PATH anda
```shell
sudo mkdir -p /usr/local/bin/
sudo install minikube /usr/local/bin/
```
4. Jika anda belum mempunyai docker grup, silahkan anda tambahkan
```shell
sudo groupadd docker
```
5. Jika anda belum mempunyai sebuah user, silahkan buat
```shell
sudo adduser $USER
```
6. Anda dapat menambahkan pengguna yang telah dibuat ke docker grup
```shell
sudo usermod -aG docker $USER
```
7. Silahkan Logout dan jalankan images tanpa menggunakan parameter sudo
```shell
docker run hello-world
```
8. Silahkan jalankan minikube 
```shell
minikube start
```
9. Silahkan cek status minikube
```shell
minikube status
```

Semoga bermanfaat