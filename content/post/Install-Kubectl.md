+++
author = "Zuhair Yahya"
title = "Install Kubectl"
date = "2021-02-22"
description = "Cara Install Kubectl"
tags = [
    "Kubectl",
    "Linux",
    "Container",
    "Kubernetes",
]
categories = [
    "DevOps",
]
+++

Pada artikel kedua ini akan dijelaskan bagaimana cara menginstall kubectl

## Install Kubectl

1. Install Kubectl dengan menggunakan Curl
```shell
curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.18.0/bin/linux/amd64/kubectl
```
2. Jadikan biner kubectl agar dapat dijalankan
```shell
chmod +x ./kubectl
```
3. Pindahkan binner kubectl kedalam PATH
```shell
sudo mv ./kubectl /usr/local/bin/kubectl
```
4. Untuk mengecek semua versi kubectl
```shell
kubectl version
```
5. Untuk mengecek versi client kubectl
```shell
kubectl version --client
```
6. Untuk melihat detail koneksi cluster
```shell
kubectl config view
```
7. Untuk melihat info cluster
```shell
kubectl cluster-info
```
8. Untuk melihat semua Node di dalam namespace
```shell
kubectl get node
```
9. Untuk melihat detail Node di dalam namespace
```shell
kubectl describe node *nama node*
```

Mungkin itu saja yang bisa dijelaskan, semoga bermanfaat.