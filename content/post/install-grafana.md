+++
author = "Zuhair Yahya"
title = "Install Grafana"
date = "2021-02-23"
description = "Cara Install Grafana"
tags = [
    "Grafana",
    "Monitoring",
]
categories = [
    "DevOps",
]
+++

Pada artikel ketiga ini akan dibahas mengenai cara menginstall grafana menggunakan centos 7 untuk kebutuhan monitoring

## Install Grafana

1.  Tambahkan Grafana RPM repository ke dalam sistem
```shell
sudo tee  /etc/yum.repos.d/grafana.repo<<EOF
[grafana]
name=grafana
baseurl=https://packages.grafana.com/oss/rpm
repo_gpgcheck=1
enabled=1
gpgcheck=1
gpgkey=https://packages.grafana.com/gpg.key
sslverify=1
sslcacert=/etc/pki/tls/certs/ca-bundle.crt
EOF
```
2. Silahkan buat repo
```shell
vi /etc/yum.repos.d/grafana.repo
```
```shell
[grafana]
name=grafana
baseurl=https://packages.grafana.com/oss/rpm
repo_gpgcheck=1
enabled=1
gpgcheck=1
gpgkey=https://packages.grafana.com/gpg.key
sslverify=1
sslcacert=/etc/pki/tls/certs/ca-bundle.crt
```
3. Install Grafana
```shell
yum install grafana
```
4. Reload daemon
```shell
systemctl daemon-reload
```
5. Start Grafana
```shell
systemctl start grafana-server
```
6. Cek status grafana
```shell
systemctl status grafana-server
```
7. Enable grafana
```shell
systemctl enable grafana-server
```
8. Buka port firewall tcp 3000
```shell
iptables -A INPUT -p tcp 3000 -j ACCEPT
```
9. Simpan iptables
```shell
service iptables save
```
10. Cek menggunakan web browser menggunakan url
```shell
http://Ipserver_grafana:3000/
```

Mungkin itu saja yang bisa dijelaskan, semoga bermanfaat