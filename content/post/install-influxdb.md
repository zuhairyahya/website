+++
author = "Zuhair Yahya"
title = "Install InfluxDB"
date = "2021-02-23"
description = "Cara Install InfluxDB"
tags = [
    "Grafana",
    "Monitoring",
    "InfluxDB",
]
categories = [
    "DevOps",
]
+++

Pada artikel keempat ini akan menjelaskan mengenai cara menginstall influxdb 

# Install InfluxDB

1. Tambahkan influxdb kedalam repository
```shell
cat <<EOF | sudo tee /etc/yum.repos.d/influxdb.repo
[influxdb]
name = InfluxDB Repository - RHEL \$releasever
baseurl = https://repos.influxdata.com/rhel/\$releasever/\$basearch/stable
enabled = 1
gpgcheck = 1
gpgkey = https://repos.influxdata.com/influxdb.key
EOF
```
2. Update cache
```shell
yum makecache fast
```
3. Install influxdb
```shell
yum -y influxdb vim curl
```
4. Start influxdb
```shell
systemctl start influxdb
```
5. Enable influxdb
```shell
systemctl enable influxdb
```
6. Buka port firewall tcp 8086
```shell
iptables -A INPUT -p tcp 8086 -j ACCEPT
```
7. Simpan iptables
```shell
service iptables save
```
Mungkin itu saja yang bisa jelaskan, semoga bermanfaat untuk anda