+++
author = "Zuhair Yahya"
title = "Install Openstack With Devstack"
date = "2021-04-07"
description = "Cara Install Openstack With Devstack"
tags = [
    "Openstack",
    "Devstack",
]
categories = [
    "DevOps",
]
+++

Pada artikel keenam ini, saya akan mencoba untuk menjelaskan mengenai cara menginstall Openstack pada ubuntu 18.04 dengan Devstack

## Install Openstack Devstack

1. Tambahkan ip pada hosts 
```shell
sudo vi /etc/hosts
```

2. Update packages
```shell
sudo apt update
```

3. Buat user bernama stack
```shell
sudo useradd -s /bin/bash -d /opt/stack -m stack
```

4. Mengaktifkan sudo tanpa menggunakan password
```shell
echo "stack ALL=(ALL) NOPASSWD: ALL" | sudo tee /etc/sudoers.d/stack
```

5. Masuk sebagai user stack
```shell
sudo su - stack
```

6. Install Git
```shell
sudo apt -y install git
```

7. Clone devstack
```shell
git clone https://opendev.org/openstack/devstack
```

8. Masuk directory devstack
```shell
cd devstack
```

9. Buat file local.conf
```shell
sudo nano local.conf

[[local|localrc]]
# Password untuk KeyStone, Database, RabbitMQ dan Service
ADMIN_PASSWORD=(sesusaikan)
DATABASE_PASSWORD=$ADMIN_PASSWORD
RABBIT_PASSWORD=$ADMIN_PASSWORD
SERVICE_PASSWORD=$ADMIN_PASSWORD
# Host IP - IP VM yang telah dikonfigurasi sebelumnya (cek : ifconfig)
HOST_IP=(ip address sesuaikan)
```

9. Deploy openstack devstack
```shell
./stack.sh
```

10. Jika sudah akses melalui browser
```shell
http://ip-address/dashboard
```

Semoga bermanfaat untuk anda