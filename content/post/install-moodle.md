+++
author = "Zuhair Yahya"
title = "Install Moodle"
date = "2021-04-14"
description = "Cara Install Moodle"
tags = [
    "Linux",
]
categories = [
    "Linux",
]
+++

Pada artikel keenam ini, saya akan menjelaskan cara menginstall moodle pada ubuntu 18.04

## Install Moodle

1. Update packages
```shell
sudo apt-get update
```
2. Install apache
```shell
sudo apt install apache2
```
3. Start apache
```shell
sudo systemctl start apache2
```
4. Cek status apache
```shell
sudo systemctl status apache2
```
5. Install mysql client
```shell
sudo apt install mysql-client mysql-server php libapache2-mod-php
```
6. Start mysql
```shell
sudo systemctl start mysql
```
7. Enable mysql
```shell
sudo systemctl enable mysql
```
8. Security mysql
```shell
sudo mysql_secure_installation

- Untuk masukkan password, tekan enter saja
- masukkan password baru
- Sisanya pilih Y semua
```
9. Edit file mysqld.cnf
```shell
sudo nano /etc/mysql/mysql.conf.d/mysqld.cnf

Pada bagian bawah [mysql]
default_storage_engine = innodb
innodb_file_per_table = 1
innodb_file_format = Barracuda
innodb_large_prefix = 1
```
10. Restart mysql
```shell
sudo systemctl restart mysql
```
11. Install php 7.2
```shell
sudo apt install graphviz aspell ghostscript clamav php7.2-pspell php7.2-curl php7.2-gd php7.2-intl php7.2-mysql php7.2-xml php7.2-xmlrpc php7.2-ldap php7.2-zip php7.2-soap php7.2-mbstring
```
12. Buat database
```shell
sudo mysql -u root -p

CREATE DATABASE moodle;
CREATE USER 'moodle'@'localhost'  IDENTIFIED BY  'masukkan-password';
ALTER DATABASE moodle DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
GRANT SELECT,INSERT,UPDATE,DELETE,CREATE,CREATE TEMPORARY TABLES,DROP,INDEX,ALTER ON moodle.* TO moodle@localhost IDENTIFIED BY 'masukkan-password';
FLUSH PRIVILEGES;
EXIT;
```
13. Download moodle versi 3.4
```shell
wget https://download.moodle.org/download.php/direct/stable34/moodle-latest-34.tgz
```
14. Ekstrak file moodle
```shell
tar -zxvf moodle-latest-34.tgz
```
15. Pindahkan file moodle
```shell
sudo mv moodle /var/www/html/moodle
```
16. Buat directory moodle
```shell
sudo mkdir /var/www/moodledata
```
17. Berikan hak akses
```shell
sudo chown -R www-data:www-data /var/www/html/moodle/
sudo chmod -R 755 /var/www/html/moodle/
sudo chown www-data /var/www/moodledata
sudo chmod -R 777 /var/www/moodledata
```
18. Buat file moodle.conf
```shell
<VirtualHost *:80>
ServerAdmin admin@example.com
DocumentRoot /var/www/html/moodle/
ServerName example.com
ServerAlias www.example.com

<Directory /var/www/html/moodle/>
Options +FollowSymlinks
AllowOverride All
Require all granted
</Directory>

ErrorLog ${APACHE_LOG_DIR}/error.log
CustomLog ${APACHE_LOG_DIR}/access.log combined

</VirtualHost>
```
19. Mengaktifkan virtualhost
```shell
sudo a2enmod rewrite
sudo a2ensite moodle.conf
```
20. Restart apache
```shell
sudo systemctl restart apache2
```
21. Buka browser dan ketikkan ip anda
```shell
http://ip-address/moodle
```

Mungkin itu saja yang bisa saja jelaskan, semoga bermanfaat untuk anda