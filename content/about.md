+++
title = "About"
description = "Documentation Zuhair Yahya"
date = "2021-02-15"
aliases = ["about-us", "contact"]
author = "Zuhair Yahya"
+++

This website is my documentation regarding my curiosity about the world of Backend Engineers and DevOps Engineers. I am currently working at a startup company as a Cloud Engineer.

You can check my documentation and experience:

* https://linkedin.com/in/zuhair-yahya-549b80156/
* https://github.com/ZuhairYahya

Hopefully this documentation can help friends in solving problems and gaining insight
